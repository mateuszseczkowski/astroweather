package mateusz.models;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class IsNetworkConnection {

    boolean isNetworkConnection = false;

    public IsNetworkConnection(Context context) {
        isNetworkConnection = checkIfIssNetworkAvailable(context);
    }

    public boolean getIsNetworkConnection() {
        return isNetworkConnection;
    }

    private boolean checkIfIssNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}