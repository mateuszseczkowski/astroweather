package mateusz.models;

public class NavItem {

    private String title;
    private String subtTitle;
    private int resIcon;

    public NavItem(String title, String subtTitle, int resIcon) {
        super();
        this.title = title;
        this.subtTitle = subtTitle;
        this.resIcon = resIcon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtTitle() {
        return subtTitle;
    }

    public void setSubtTitle(String subtTitle) {
        this.subtTitle = subtTitle;
    }

    public int getResIcon() {
        return resIcon;
    }

    public void setResIcon(int resIcon) {
        this.resIcon = resIcon;
    }
}
