package mateusz.models;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AstroGetData {

    String moon = "";
    String sun = "";

    Calendar c = Calendar.getInstance();
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH) + 1;
    int day = c.get(Calendar.DAY_OF_MONTH);
    int hour = c.get(Calendar.HOUR);
    int minute = c.get(Calendar.MINUTE);
    int second = c.get(Calendar.SECOND);
    int timeZoneOffset = c.get(Calendar.DST_OFFSET) / (60 * 60 * 1000);

    boolean dayLightSave = TimeZone.getDefault().inDaylightTime(new Date());

    Double latitudeDouble;
    Double longitudeDouble;

    public AstroGetData(Double latitudeDouble, Double longitudeDouble) {
        this.latitudeDouble = latitudeDouble;
        this.longitudeDouble = longitudeDouble;
    }

    public String astroMoon() {

        AstroCalculator newAstroContent = new AstroCalculator(
                new AstroDateTime(year, month, day, hour, minute, second, timeZoneOffset, dayLightSave),
                new AstroCalculator.Location(latitudeDouble, longitudeDouble)
        );

        moon = " Moon rise:" + newAstroContent.getMoonInfo().getMoonrise().toString().substring(10,16)
                + "\n Moon set: " + newAstroContent.getMoonInfo().getMoonset().toString().substring(10,16)
                + "\n New moon: " + newAstroContent.getMoonInfo().getNextNewMoon().toString()//.substring(0, 16)
                + "\n Full moon: " + newAstroContent.getMoonInfo().getNextFullMoon().toString().substring(0, 16)
                + "\n Moon phase: " + newAstroContent.getMoonInfo().getIllumination() * 100 + "%"
                + "\n Synodic day month: " + newAstroContent.getMoonInfo().getAge();

        return  newAstroContent.getLocation().getLatitude()
                + "***" + newAstroContent.getLocation().getLongitude()
                + ":\n"+ moon;
    }

    public String astroSun() {

        AstroCalculator newAstroContent = new AstroCalculator(
                new AstroDateTime(year, month, day, hour, minute, second, timeZoneOffset, dayLightSave),
                new AstroCalculator.Location(latitudeDouble, longitudeDouble)
        );

        sun = " Sun rise:" + newAstroContent.getSunInfo().getSunrise().toString().substring(10,16)
                + "\n Azimuth:" + newAstroContent.getSunInfo().getAzimuthRise()
                + "\n Sun set:" + newAstroContent.getSunInfo().getSunset().toString().substring(10,16)
                + "\n Azimuth:" + newAstroContent.getSunInfo().getAzimuthSet()
                + "\n Twilight Morning: " + newAstroContent.getSunInfo().getTwilightMorning().toString().substring(10,16)
                + "\n Twilight Evening: " + newAstroContent.getSunInfo().getTwilightEvening().toString().substring(10,16);

        return newAstroContent.getLocation().getLatitude()
                +"***" + newAstroContent.getLocation().getLongitude()
                +":\n" + sun;
    }
}