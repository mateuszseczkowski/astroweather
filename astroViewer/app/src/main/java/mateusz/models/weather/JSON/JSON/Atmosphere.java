package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Atmosphere implements JSONPopulator{

    private double pressure;
    private double humidity;
    private double visibility;

    public double getPressure() { return pressure; }
    public double getHumidity() { return humidity; }
    public double getVisibility() { return visibility; }

    @Override
    public void populate(JSONObject jObject) {

        pressure = jObject.optDouble("pressure");
        humidity = jObject.optDouble("humidity");
        visibility = jObject.optDouble("visibility");

    }

    @Override
    public void populate(JSONArray jArray) {

    }

}
