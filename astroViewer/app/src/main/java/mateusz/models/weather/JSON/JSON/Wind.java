package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Wind implements JSONPopulator{

    private int speed;
    private int direction;

    public double getSpeed() { return speed; }
    public double getDirection() { return direction; }

    @Override
    public void populate(JSONObject jObject) {

        speed = jObject.optInt("speed");
        direction = jObject.optInt("direction");

    }

    @Override
    public void populate(JSONArray jArray) {

    }
}
