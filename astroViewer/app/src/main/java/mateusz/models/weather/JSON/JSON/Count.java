package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONObject;

public class Count implements JSONPopulator{

    private int count;

    public int getCount() {
        return count;
    }

    @Override
    public void populate(JSONObject jObject) {
        count = jObject.optInt("count");
    }

    @Override
    public void populate(JSONArray jArray) {

    }
}
