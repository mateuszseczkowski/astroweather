package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface JSONPopulator {
    void populate (JSONObject jObject);
    void populate (JSONArray jArray);
}
