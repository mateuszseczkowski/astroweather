package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Item implements JSONPopulator{
    private Condition condition;
    private Forecast forecast;
    private String title;
    private String latitude;
    private String longitude;

    public Condition getCondition() {
        return condition;
    }

    public String getTitle() {
        return title;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Forecast getForecast() { return forecast;}

    @Override
    public void populate(JSONObject jObject) {
        condition = new Condition();
        condition.populate(jObject.optJSONObject("condition"));

        forecast = new Forecast();
        forecast.populate(jObject.optJSONArray("forecast"));

        title = jObject.optString("title");
        latitude = jObject.optString("lat");
        longitude = jObject.optString("long");
    }

    @Override
    public void populate(JSONArray jArray) {

    }

}
