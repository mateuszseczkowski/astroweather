package mateusz.models.weather.JSON.YahooService;

import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import mateusz.models.weather.JSON.JSON.Channel;

public class YahooWeatherServiceFindLocationOnly {

    private YahooWeatherCallbackLocationOnly callback;
    private String location;
    private Exception error;

    public YahooWeatherServiceFindLocationOnly(YahooWeatherCallbackLocationOnly callback) {
        this.callback = callback;
    }

    public String getLocation() {
        return location;
    }

    public void findLocation (String l) {

        this.location = l;

        new AsyncTask<String, Void, String>() {

            @Override
            protected String doInBackground(String... strings) {

                String YQL = String.format("select woeid from geo.places(1) where text=\"%s\"", strings[0]);
                String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(YQL));

                try {
                    URL url = new URL(endpoint);
                    URLConnection connection = url.openConnection();
                    InputStream inputStream = connection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line=bufferedReader.readLine())!=null) {
                        result.append(line);
                    }

                    return result.toString();

                } catch (Exception e) {
                    error = e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {

                if (s == null && error != null) {
                    callback.serviceFail(error);
                    return;
                }

                try {
                    JSONObject jObject = new JSONObject(s);

                    JSONObject query = jObject.optJSONObject("query");
                    int count = query.optInt("count");
                    if (count == 0) {
                        callback.serviceFail(new LocationWeatherException("Wrong location " + location));
                        return;
                    }

                    callback.serviceSuccess(location);

                } catch (JSONException e) {
                    callback.serviceFail(e);
                }
            }
        }.execute(location);
    }

    public class LocationWeatherException extends Exception {
        public LocationWeatherException(String detailMessage) {
            super(detailMessage);
        }
    }
}
