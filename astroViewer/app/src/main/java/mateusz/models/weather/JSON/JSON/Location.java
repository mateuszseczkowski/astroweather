package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Location implements JSONPopulator{

    private String city;
    private String country;
    private String region;

    public String getLocation() { return city + ", " + country + ", " + region +" Voivodeship"; }

    @Override
    public void populate(JSONObject jObject) {

        city = jObject.optString("city");
        country = jObject.optString("country");
        region = jObject.optString("region");
    }

    @Override
    public void populate(JSONArray jArray) {

    }

}
