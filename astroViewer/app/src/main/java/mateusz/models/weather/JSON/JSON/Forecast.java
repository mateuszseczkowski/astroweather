package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Forecast implements JSONPopulator{

    private int[] code = new int[9];
    private int[] high = new int[9];
    private int[] low = new int[9];
    private String[] date = new String[9];
    private String[] day = new String[9];
    private String[] text = new String[9];

    public int getCode(int i) {
        return code[i];
    }
    public int getHigh(int i) {return high[i];}
    public int getLow(int i) {return low[i];}

    public String getText(int i) {
        return text[i];
    }
    public String getDate(int i) {
        return date[i];
    }
    public String getDay(int i) {
        return day[i];
    }

    @Override
    public void populate(JSONObject jObject) {
    }

    @Override
    public void populate(JSONArray jArray) {

        for (int i=0; i < 7; i++)
        {
            try {
                code[i] = jArray.getJSONObject(i).optInt("code");
                high[i] = jArray.getJSONObject(i).optInt("high");
                low[i] = jArray.getJSONObject(i).optInt("low");
                date[i] = jArray.getJSONObject(i).optString("date");
                day[i] = jArray.getJSONObject(i).optString("day");
                text[i] = jArray.getJSONObject(i).optString("text");
            } catch (JSONException e) {
                // Oops
            }
        }
    }
}
