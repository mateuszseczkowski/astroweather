package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Condition implements JSONPopulator{

    private int code;
    private int temperature;
    private String description;

    public int getCode() {
        return code;
    }

    public int getTemperature() {return temperature;}

    public String getDescription() {
        return description;
    }

    @Override
    public void populate(JSONObject jObject) {

        code = jObject.optInt("code");
        temperature = jObject.optInt("temp");
        description = jObject.optString("text");

    }

    @Override
    public void populate(JSONArray jArray) {

    }

}
