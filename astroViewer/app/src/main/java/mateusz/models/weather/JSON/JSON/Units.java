package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Units implements JSONPopulator{

    private String temperatureUnits;
    private String pressureUnits;
    private String speedUnits;
    private String distanceUnits;

    public String getTemperatureUnits() {
        return temperatureUnits;
    }

    public String getPressureUnits() {
        return pressureUnits;
    }

    public String getSpeedUnits() {return speedUnits;}

    public String getDistanceUnits() {return distanceUnits;}

    @Override
    public void populate(JSONObject jObject) {
        temperatureUnits = jObject.optString("temperature");
        pressureUnits = jObject.optString("pressure");
        speedUnits = jObject.optString("speed");
        distanceUnits = jObject.optString("distance");
    }

    @Override
    public void populate(JSONArray jArray) {

    }

}
