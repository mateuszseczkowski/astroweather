package mateusz.models.weather.JSON.YahooService;

import org.json.JSONObject;

import mateusz.models.weather.JSON.JSON.Channel;

public interface YahooWeatherCallbackLocationOnly {

    void serviceSuccess(String location);
    void serviceFail(Exception exception);

}
