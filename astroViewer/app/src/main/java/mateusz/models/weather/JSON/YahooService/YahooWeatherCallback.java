package mateusz.models.weather.JSON.YahooService;

import mateusz.models.weather.JSON.JSON.Channel;

public interface YahooWeatherCallback {

    void serviceSuccess (Channel channel);
    void serviceFail (Exception exception);

}
