package mateusz.models.weather.JSON.JSON;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Channel implements JSONPopulator{

    private Units units;
    private Item item;
    private Atmosphere atmosphere;
    private Location location;
    private Wind wind;

    public Units getUnits() {
        return units;
    }

    public Item getItem() {
        return item;
    }

    public Atmosphere getAtmoshpere() {
        return atmosphere;
    }

    public Location getLocation() {
        return location;
    }

    public Wind getWind() {
        return wind;
    }

    @Override
    public void populate(JSONObject jObject) {

        units = new Units();
        units.populate(jObject.optJSONObject("units"));
        item = new Item();
        item.populate(jObject.optJSONObject("item"));
        atmosphere = new Atmosphere();
        atmosphere.populate(jObject.optJSONObject("atmosphere"));
        location = new Location();
        location.populate(jObject.optJSONObject("location"));
        wind = new Wind();
        wind.populate(jObject.optJSONObject("wind"));
    }

    @Override
    public void populate(JSONArray jArray) {

    }
}
