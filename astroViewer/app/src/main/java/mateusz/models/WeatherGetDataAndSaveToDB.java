package mateusz.models;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import mateusz.astro.adapters.DBHandler;
import mateusz.models.weather.JSON.JSON.Atmosphere;
import mateusz.models.weather.JSON.JSON.Channel;
import mateusz.models.weather.JSON.JSON.Item;
import mateusz.models.weather.JSON.JSON.Location;
import mateusz.models.weather.JSON.JSON.Wind;

public class WeatherGetDataAndSaveToDB {

    Channel channel;
    Context context;

    public WeatherGetDataAndSaveToDB(Channel channel, Context context) {
        this.channel = channel;
        this.context = context;
    }

    public void getDataAndSaveInDB() {

        DBHandler db = new DBHandler(context);
        Item item = channel.getItem();

        //basic
        try {

            Atmosphere atmosphere = channel.getAtmoshpere();
            Location location = channel.getLocation();
            String textLatLong = "lat: " + item.getLatitude() + "\u00B0" +" | long: "+ item.getLongitude() + "\u00B0";
            String textTemperature = item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperatureUnits();
            String textPressure = atmosphere.getPressure() + " " + channel.getUnits().getPressureUnits();

            String textPressureInhPa = "";
            if (channel.getUnits().getPressureUnits().contains("mb")) {
                Double value = atmosphere.getPressure()/33.063172;
                value = round(value, 2);
                textPressureInhPa = "" + value + " hPa";
            }

            int resourceId = context.getResources().getIdentifier("drawable/weezle_"+ item.getCondition().getCode(), null, context.getPackageName());

            if (db.getCountBasic() > 0) {
                db.updateValue(0, location.getLocation(),textLatLong ,item.getTitle(), textTemperature, item.getCondition().getDescription(),
                        textPressure, textPressureInhPa ,resourceId);
            }
            else {
                db.addData(location.getLocation(),textLatLong ,item.getTitle(), textTemperature,item.getCondition().getDescription(),
                        textPressure, textPressureInhPa ,resourceId);
            }

        } catch (Exception e) {
            Log.e("ERROR WITH GETTING DATA", " Something wrong with BASIC DATA " + e.toString());
        }

        //additional
        try {
            Atmosphere atmosphere = channel.getAtmoshpere();
            Wind wind = channel.getWind();

            String textSpeed = "Speed: " + wind.getSpeed() + " " + channel.getUnits().getSpeedUnits();
            String textDirection = "Direction: " + wind.getDirection() + "\u00B0";
            String textHumidity = "Humidity: " + atmosphere.getHumidity() + " %";
            String textVisibility = "Visibility: " + atmosphere.getVisibility() + " " + channel.getUnits().getDistanceUnits();

            int resourceId = context.getResources().getIdentifier("drawable/wind_flag", null, context.getPackageName());

            if (db.getCountAdditional() > 0) {
                db.updateValueAdditional(1, textSpeed, textDirection, textHumidity, textVisibility, resourceId);
            }
            else {
                db.addDataAdditional(textSpeed, textDirection, textHumidity, textVisibility, resourceId);
            }

        } catch (Exception e) {
            Log.e("ERROR WITH GETTING DATA", " Something wrong with ADDITIONAL DATA " + e.toString());
        }

        //next 5 days
        try {

            for (int i = 0; i < 5; i++) {

                String textDayText = switchDayMap(item.getForecast().getDay(i)) + " will be " + item.getForecast().getText(i);
                String textHighLow = "High will be: " + item.getForecast().getHigh(i) + "\u00B0" + channel.getUnits().getTemperatureUnits()
                        + " Low will be: " + item.getForecast().getLow(i) + "\u00B0" + channel.getUnits().getTemperatureUnits();
                String textDate = item.getForecast().getDate(i);

                int resourceId = context.getResources().getIdentifier("drawable/weezle_" + item.getForecast().getCode(i), null, context.getPackageName());

                if (db.getCountNextDays() > 5) {
                    db.updateValueNextDays(i + 1, textDayText, textHighLow, textDate, resourceId);
                } else {
                    db.addDataNextDays(i + 1, textDayText, textHighLow, textDate, resourceId);
                }

            }

        } catch (Exception e) {
            Log.e("ERROR WITH GETTING DATA", " Something wrong with NEXT DAYS DATA " + e.toString());
        }
    }

    private String switchDayMap(String day) {

        Map<String,String> map = new HashMap<String,String>(7);

        map.put("Mon", "Monday");
        map.put("Tue", "Tuesday");
        map.put("Wed", "Wednesday");
        map.put("Thu", "Thursday");
        map.put("Fri", "Friday");
        map.put("Sat", "Saturday");
        map.put("Sun", "Sunday");

        return map.get(day);
    }

    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
