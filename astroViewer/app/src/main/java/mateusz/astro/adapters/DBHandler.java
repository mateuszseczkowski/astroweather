package mateusz.astro.adapters;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "WeatherDB";
    private static final String TABLE_WEATHER = "weather";
    private static final String TABLE_WEATHER_ADDITIONAL = "weatheradditional";
    private static final String TABLE_WEATHER_ND = "weathernextdays";
    private static final String TABLE_CITIES = "cities";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_LOCATION = "location";
    private static final String KEY_LAT_LONG = "latlong";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TEMPERATURE = "temperature";
    private static final String KEY_CONDITION = "condition";
    private static final String KEY_PRESSURE = "pressure";
    private static final String KEY_PRESSURE_HPA = "pressurehpa";
    private static final String KEY_RESOURCE = "resource";
    private static final String KEY_WIND_SPEED = "windSpeed";
    private static final String KEY_DIRECTION = "direction";
    private static final String KEY_HUMIDITY = "humidity";
    private static final String KEY_VISIBILITY = "visibility";
    private static final String KEY_DayText_ND = "daytextnd";
    private static final String KEY_HighLow_ND = "highlownd";
    private static final String KEY_DATE_ND = "datend";
    private static final String KEY_CITY_NAME = "cityname";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_WEATHER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_WEATHER + " ( "
        + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
        + KEY_LOCATION + " TEXT, "
        + KEY_LAT_LONG + " TEXT, "
        + KEY_TITLE + " TEXT, "
        + KEY_TEMPERATURE + " TEXT, "
        + KEY_CONDITION + " TEXT, "
        + KEY_PRESSURE + " TEXT, "
        + KEY_PRESSURE_HPA + " TEXT, "
        + KEY_RESOURCE + " INTEGER " + ")";

        String CREATE_WEATHER_TABLE_ADDITIONAL = "CREATE TABLE IF NOT EXISTS " + TABLE_WEATHER_ADDITIONAL + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
                + KEY_WIND_SPEED + " TEXT, "
                + KEY_DIRECTION + " TEXT, "
                + KEY_HUMIDITY + " TEXT, "
                + KEY_VISIBILITY + " TEXT, "
                + KEY_RESOURCE + " INTEGER " + ")";

        String CREATE_WEATHER_TABLE_ND = "CREATE TABLE IF NOT EXISTS " + TABLE_WEATHER_ND + " ( "
                    + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
                    + KEY_DayText_ND + " TEXT, "
                    + KEY_HighLow_ND + " TEXT, "
                    + KEY_DATE_ND + " TEXT,  "
                    + KEY_RESOURCE + " INTEGER " + ")";

        String CREATE_CITY = "CREATE TABLE IF NOT EXISTS " + TABLE_CITIES + " ( "
                        + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " TEXT, "
                        + KEY_CITY_NAME + " TEXT " + ")";

        db.execSQL(CREATE_WEATHER_TABLE);
        db.execSQL(CREATE_WEATHER_TABLE_ADDITIONAL);
        db.execSQL(CREATE_WEATHER_TABLE_ND);
        db.execSQL(CREATE_CITY);

        Log.i("SUCCESS", "Four tables has been added successfully");

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER_ADDITIONAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER_ND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CITIES);
        onCreate(db);
    }

    public void addData(String location, String latLong  ,String title, String temperature, String condition, String pressure, String pressureHPA,int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, "0");
        values.put(KEY_LOCATION, location);
        values.put(KEY_LAT_LONG, latLong);
        values.put(KEY_TITLE, title);
        values.put(KEY_TEMPERATURE, temperature);
        values.put(KEY_CONDITION, condition);
        values.put(KEY_PRESSURE, pressure);
        values.put(KEY_PRESSURE_HPA, pressureHPA);
        values.put(KEY_RESOURCE, resourceId);

        db.insert(TABLE_WEATHER, null, values);
    }

    public void addDataAdditional(String windSpeed, String direction, String humidity, String visibility, int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, "1");
        values.put(KEY_WIND_SPEED, windSpeed);
        values.put(KEY_DIRECTION, direction);
        values.put(KEY_HUMIDITY, humidity);
        values.put(KEY_VISIBILITY, visibility);
        values.put(KEY_RESOURCE, resourceId);

        db.insert(TABLE_WEATHER_ADDITIONAL,  null, values);
    }

    public void addDataNextDays(int id, String dayText, String highLow, String date, int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, id);
        values.put(KEY_DayText_ND, dayText);
        values.put(KEY_HighLow_ND, highLow);
        values.put(KEY_DATE_ND, date);
        values.put(KEY_RESOURCE, resourceId);

        db.insert(TABLE_WEATHER_ND,  null, values);
    }

    public void addDataCity(int id, String cityName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, id);
        values.put(KEY_CITY_NAME, cityName);

        db.insert(TABLE_CITIES,  null, values);
    }

    public String getLocation(int id) {
        return getData(id, KEY_LOCATION, TABLE_WEATHER );
    }

    public String getCoordinates(int id) {
        return getData(id, KEY_LAT_LONG, TABLE_WEATHER );
    }

    public String getTitle(int id) {
        return getData(id, KEY_TITLE, TABLE_WEATHER);
    }

    public String getTemperature(int id) {
        return getData(id, KEY_TEMPERATURE, TABLE_WEATHER);
    }

    public String getCondition(int id) {
        return getData(id, KEY_CONDITION, TABLE_WEATHER);
    }

    public String getPressure(int id) {
        return getData(id, KEY_PRESSURE, TABLE_WEATHER);
    }

    public String getPressureHPA (int id)  {
        return getData(id, KEY_PRESSURE_HPA, TABLE_WEATHER);
    }

    public int getResourceId(int id) {
        return Integer.parseInt(getData(id, KEY_RESOURCE, TABLE_WEATHER));
    }

    public String getWindSpeed(int id) {
        return getData(id, KEY_WIND_SPEED, TABLE_WEATHER_ADDITIONAL );
    }

    public String getDirection(int id) {
        return getData(id, KEY_DIRECTION, TABLE_WEATHER_ADDITIONAL);
    }

    public String getHumidity(int id) {
        return getData(id, KEY_HUMIDITY, TABLE_WEATHER_ADDITIONAL);
    }

    public String getVisibility(int id) {
        return getData(id, KEY_VISIBILITY, TABLE_WEATHER_ADDITIONAL);
    }

    public int getResourceIdAdditional(int id) {
        return Integer.parseInt(getData(id, KEY_RESOURCE, TABLE_WEATHER_ADDITIONAL));
    }

    public String getDayText(int id) {
        return getData(id, KEY_DayText_ND, TABLE_WEATHER_ND);
    }

    public String getHighLow(int id) {
        return getData(id, KEY_HighLow_ND, TABLE_WEATHER_ND);
    }

    public String getDate(int id) {
        return getData(id, KEY_DATE_ND, TABLE_WEATHER_ND);
    }

    public int getResourceIdNextDays(int id) {
        return Integer.parseInt(getData(id, KEY_RESOURCE, TABLE_WEATHER_ND));
    }

    public String getCity(String value) {
        return getDataByColumnValue(value, KEY_CITY_NAME, TABLE_CITIES);
    }

    private String getData (int id, String columnName, String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName, new String[]{KEY_ID,
                        KEY_NAME, columnName}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        String result = "";
        if (cursor != null) {
            cursor.moveToFirst();
            result = cursor.getString(2);
        }
        return result;
    }

    private String getDataByColumnValue (String value, String columnName, String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName, new String[]{KEY_ID,
                        KEY_NAME, columnName}, columnName + "=?",
                new String[]{value}, null, null, null, null);
        String result = "";
        if (cursor != null) {
            cursor.moveToFirst();
            result = cursor.getString(2);
        }
        return result;
    }

    public int updateValue(int id, String location, String latLong ,String title, String temperature, String condition, String pressure,String pressureHPA , int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, id);
        values.put(KEY_LOCATION, location);
        values.put(KEY_LAT_LONG, latLong);
        values.put(KEY_TITLE, title);
        values.put(KEY_TEMPERATURE, temperature);
        values.put(KEY_CONDITION, condition);
        values.put(KEY_PRESSURE, pressure);
        values.put(KEY_PRESSURE_HPA, pressureHPA);

        values.put(KEY_RESOURCE, resourceId);

        return db.update(TABLE_WEATHER, values, KEY_NAME + " = ?",
                new String[]{String.valueOf(id)});
    }

    public int updateValueAdditional(int id, String windSpeed, String direction, String humidity, String visibility, int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, id);
        values.put(KEY_WIND_SPEED, windSpeed);
        values.put(KEY_DIRECTION, direction);
        values.put(KEY_HUMIDITY, humidity);
        values.put(KEY_VISIBILITY, visibility);
        values.put(KEY_RESOURCE, resourceId);

        return db.update(TABLE_WEATHER_ADDITIONAL, values, KEY_NAME + " = ?",
                new String[]{String.valueOf(id)});
    }

    public int updateValueNextDays(int id, String dayText, String highLow, String date, int resourceId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, id);
        values.put(KEY_DayText_ND, dayText);
        values.put(KEY_HighLow_ND, highLow);
        values.put(KEY_DATE_ND, date);
        values.put(KEY_RESOURCE, resourceId);

        return db.update(TABLE_WEATHER_ND, values, KEY_NAME + " = ?",
                new String[]{String.valueOf(id)});
    }

    public void delete(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WEATHER, KEY_ID +"=?",
            new String[] { String.valueOf(id) });
        db.close();
    }

    public void deleteCity(String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CITIES, KEY_CITY_NAME +" = ?",
                new String[] { value });
        db.close();
    }

    public int getCountBasic() {
        return getCount(TABLE_WEATHER);
    }

    public int getCountAdditional() {
        return getCount(TABLE_WEATHER_ADDITIONAL);
    }

    public int getCountNextDays() {
        return getCount(TABLE_WEATHER_ND);
    }

    private int getCount(String tableName) {
        String countQuery = "SELECT * FROM " + tableName;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int getCountCity(String value) {
        String countQuery = "SELECT * FROM " + TABLE_CITIES + " WHERE " + KEY_CITY_NAME + " LIKE " + "'"+value+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public ArrayList<String> getCitiesList() {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery =  "SELECT " +
                KEY_CITY_NAME +
                " FROM " + TABLE_CITIES;

        ArrayList<String> citiesList = new ArrayList<String>();

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                citiesList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        return citiesList;
    }

}
