package mateusz.astro.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mateusz.astro.astroViewerMainPackage.R;
import mateusz.models.NavItem;

public class AstroNavListAdapter extends ArrayAdapter<NavItem>{

    Context context;
    int resLayout;
    List<NavItem> navItemList;

    public AstroNavListAdapter(Context context, int resLayout, List<NavItem> navItemList) {
        super(context, resLayout, navItemList);
        this.context = context;
        this.resLayout = resLayout;
        this.navItemList = navItemList;
    }

    @SuppressLint("ViewHolder") @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context, resLayout, null);

        TextView tvTitle = (TextView) v.findViewById(R.id.title);
        TextView tvSubTitle = (TextView) v.findViewById(R.id.subtitle);
        ImageView navIcon = (ImageView) v.findViewById(R.id.nav_icon);

        NavItem navItem = navItemList.get(position);

        tvTitle.setText(navItem.getTitle());
        tvSubTitle.setText(navItem.getSubtTitle());
        navIcon.setImageResource(navItem.getResIcon());

        return v;
    }
}
