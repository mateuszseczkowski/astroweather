package mateusz.astro.activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import mateusz.astro.adapters.AstroNavListAdapter;
import mateusz.astro.fragments.MyAbout;
import mateusz.astro.fragments.MyHome;
import mateusz.astro.fragments.MySettings;
import mateusz.astro.astroViewerMainPackage.R;
import mateusz.models.AstroGetData;
import mateusz.models.NavItem;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

public class MainActivity extends ActionBarActivity{

	private DrawerLayout drawerLayout;
	private RelativeLayout drawerPanel;
	private ListView listView;

	List<NavItem> navItemList;
	List<Fragment> fragmentList;
	public static String PACKAGE_NAME;

	ActionBarDrawerToggle actionBarDrawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

		/*//To clean Data in phone while debuging
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("Longitude","0.0");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
				editor.apply();
		}
		editor.putString("Latitude","0.0");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			editor.apply();
		}
		*/

	//	astroDataRefresh();
		PACKAGE_NAME = getApplicationContext().getPackageName();

		this.initDrawerPanel();

		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);
	}

	private void initDrawerPanel() {

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerPanel = (RelativeLayout) findViewById(R.id.drawer_panel);
		listView = (ListView) findViewById(R.id.nav_list);

		navItemList = new ArrayList<NavItem>();

		navItemList.add(new NavItem("Home", "Weather/Moon/Sun",
				R.drawable.ic_home));

		navItemList.add(new NavItem("Settings", "",
				R.drawable.ic_settings));

		navItemList.add(new NavItem("About", "Author",
				R.drawable.ic_about));

		AstroNavListAdapter navListAdapter = new AstroNavListAdapter(getApplicationContext(),
				R.layout.item_nav_list, navItemList);

		listView.setAdapter(navListAdapter);

		fragmentList = new ArrayList<Fragment>();


		fragmentList.add(new MyHome());
		fragmentList.add(new MySettings());
		fragmentList.add(new MyAbout());

		//load first as default
		FragmentManager	fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.main_content,
				fragmentList.get(0)).commit();

		setTitle(navItemList.get(0).getTitle());
		listView.setItemChecked(0, true);
		drawerLayout.closeDrawer(drawerPanel);

		//lister for nav list buttons
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				FragmentManager	fragmentManager = getSupportFragmentManager();
				fragmentManager
						.beginTransaction()
						.replace(R.id.main_content, fragmentList.get(position))
						.commit();

				setTitle(navItemList.get(position).getTitle());
				listView.setItemChecked(position, true);
				drawerLayout.closeDrawer(drawerPanel);
			}
		});

		//listener for showing drawer panel
		actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.string.drawer_opened, R.string.drawer_closed) {
			@Override
			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu();
				super.onDrawerOpened(drawerView);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				invalidateOptionsMenu();
				super.onDrawerClosed(drawerView);
			}
		};

		drawerLayout.setDrawerListener(actionBarDrawerToggle);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		actionBarDrawerToggle.syncState();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (actionBarDrawerToggle.onOptionsItemSelected(item))
			return true;
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_MENU) {

			if (!drawerLayout.isDrawerOpen(drawerPanel)) {
				drawerLayout.openDrawer(drawerPanel);
			} else if (drawerLayout.isDrawerOpen(drawerPanel)) {
				drawerLayout.closeDrawer(drawerPanel);
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
