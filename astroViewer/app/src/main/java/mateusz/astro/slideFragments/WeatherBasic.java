package mateusz.astro.slideFragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import mateusz.astro.astroViewerMainPackage.R;
import mateusz.astro.adapters.DBHandler;
import mateusz.models.weather.JSON.YahooService.YahooWeatherService;

public class WeatherBasic extends Fragment {

	static TextView temperatureTextView;
	static TextView conditionTextView, pressureTextView, pressureHPATextView;
	static TextView locationTextView,coordinatesTextView , titleTextView;
	static SharedPreferences preferences;
	static View viewWeatherBasic;
	static ImageView weatherIconBasic;
	static YahooWeatherService service;
	static ProgressDialog progressDialog;

	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		viewWeatherBasic = inflater.inflate(R.layout.pager_fragment_basic_weather_today, container, false);
		weatherRefreshBasicDataFragment(getActivity());

		return viewWeatherBasic;
	}

	public static void weatherRefreshBasicDataFragment(Context context) {

		try{

			weatherIconBasic = (ImageView) viewWeatherBasic.findViewById(R.id.weatherIconImageView);
			locationTextView = (TextView) viewWeatherBasic.findViewById(R.id.locationTextView);
			coordinatesTextView = (TextView) viewWeatherBasic.findViewById(R.id.coordinates);
			titleTextView = (TextView) viewWeatherBasic.findViewById(R.id.titleTextView);
			temperatureTextView = (TextView) viewWeatherBasic.findViewById(R.id.temperatureTextView);
			conditionTextView = (TextView) viewWeatherBasic.findViewById(R.id.conditionTextView);
			pressureTextView = (TextView) viewWeatherBasic.findViewById(R.id.pressureTextView);
			pressureHPATextView = (TextView) viewWeatherBasic.findViewById(R.id.pressureInhPA);


			DBHandler db = new DBHandler(context);

			String location = db.getLocation(1),
					coordinates = db.getCoordinates(1),
					title = db.getTitle(1),
					temperature = db.getTemperature(1),
					condition = db.getCondition(1),
					pressure = db.getPressure(1),
					pressureInHPA = db.getPressureHPA(1);

			Drawable weatherIconDrawable = context.getResources().getDrawable(db.getResourceId(1));

			weatherIconBasic.setImageDrawable(weatherIconDrawable);
			locationTextView.setText(location);
			coordinatesTextView.setText(coordinates);
			titleTextView.setText(title);
			temperatureTextView.setText(temperature);
			conditionTextView.setText(condition);
			pressureTextView.setText(pressure);
			pressureHPATextView.setText(pressureInHPA);

			Log.i ("Conditions for ", "" + location + title + temperature + condition + pressure + pressureInHPA);

		}catch (Exception e) {

			Log.e ( "Error" , "weatherRefreshBasicDataFragment" + e.toString());

		}
	}
}