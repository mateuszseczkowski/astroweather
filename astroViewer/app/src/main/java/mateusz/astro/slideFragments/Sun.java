package mateusz.astro.slideFragments;

import mateusz.astro.astroViewerMainPackage.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Sun extends Fragment {

	static TextView myTextView;
	static SharedPreferences preferences;
	static View viewSun;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		viewSun = inflater.inflate(R.layout.pager_fragment_sun, container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		astroDataRefreshSunFragment();
//		astroDataRefreshSun();

		return viewSun;
	}

	public static void astroDataRefreshSunFragment() {

		try{
			String sunData = preferences.getString("Sun", "-");
			myTextView = (TextView) viewSun.findViewById(R.id.sunFragmentText2);
			myTextView.setText(sunData);
			myTextView.setMovementMethod(new ScrollingMovementMethod());
		}catch (Exception e) {}

	}

/*	private void astroDataRefreshSun() {

		Thread myThreadData = null;
		Runnable runnableData = new astroDataGeterSun();
		myThreadData= new Thread(runnableData);
		myThreadData.start();

	}

	class astroDataGeterSun implements Runnable{
		public void run() {
			while(!Thread.currentThread().isInterrupted()){
				try {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							try{
								Log.e("SUN", "--------------------------------------Sun fragment data refreshed---------------------------");
								String sunData = preferences.getString("Sun", "");
								myTextView = (TextView) v.findViewById(R.id.sunFragmentText2);
								myTextView.setText(sunData);

							}catch (Exception e) {}
						}
					});

					String refreshTime = preferences.getString("RefreshTime", "");
					Integer t = Integer.parseInt(refreshTime);

					Thread.sleep(1000 * 60);

				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}catch(Exception e){
				}
			}
		}
	}
*/
}
