package mateusz.astro.slideFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Queue;

import mateusz.astro.activities.MainActivity;
import mateusz.astro.adapters.DBHandler;
import mateusz.astro.astroViewerMainPackage.R;
import mateusz.models.weather.JSON.JSON.Atmosphere;
import mateusz.models.weather.JSON.JSON.Channel;
import mateusz.models.weather.JSON.JSON.Item;
import mateusz.models.weather.JSON.JSON.Wind;
import mateusz.models.weather.JSON.YahooService.YahooWeatherCallback;
import mateusz.models.weather.JSON.YahooService.YahooWeatherService;

public class WeatherNextDays extends Fragment {

	static SharedPreferences preferences;
	static View viewWeatherND;
	static YahooWeatherService service;
	static ProgressDialog progressDialog;
	static TextView[] highlow = new TextView[5], daytext = new TextView[5] , date = new TextView[5] ;
	static ImageView[] code = new ImageView[5];
	static int[] highlowArray = {R.id.highlow1,R.id.highlow2,R.id.highlow3,R.id.highlow4,R.id.highlow5};
	static int[] daytextArray = {R.id.daytext1,R.id.daytext2,R.id.daytext3,R.id.daytext4,R.id.daytext5};
	static int[] dateArray = {R.id.date1,R.id.date2,R.id.date3,R.id.date4,R.id.date5};
	static int[] codeArray = {R.id.code1,R.id.code2,R.id.code3,R.id.code4,R.id.code5};

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		viewWeatherND = inflater.inflate(R.layout.pager_fragment_basic_weather_next_days, container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		weatherRefreshNextDaysDataFragment(getActivity());

		return viewWeatherND;
	}

	public static void weatherRefreshNextDaysDataFragment(Context context) {

		try{

			for (int k = 0; k < 5 ; k ++ ) {
				highlow[k] = (TextView) viewWeatherND.findViewById(highlowArray[k]);
				daytext[k] = (TextView) viewWeatherND.findViewById(daytextArray[k]);
				date[k] = (TextView) viewWeatherND.findViewById(dateArray[k]);
				code[k] = (ImageView) viewWeatherND.findViewById(codeArray[k]);
			}

			DBHandler db = new DBHandler(context);

			for (int i = 0; i < 5; i++) {

				String dayTextText = db.getDayText(i + 1),
						highLowText = db.getHighLow(i + 1),
						dateText = db.getDate(i + 1);

				Drawable weatherIconDrawable = context.getResources().getDrawable(db.getResourceIdNextDays(i + 1));

				code[i].setImageDrawable(weatherIconDrawable);
				daytext[i].setText(dayTextText);
				highlow[i].setText(highLowText);
				date[i].setText(dateText);

				Log.i ("Conditions next days " + i + 1, "" + dayTextText + highLowText + dateText);

			}

		}catch (Exception e) {

			Log.e ( "Error" , "weatherRefreshAdditionalDataFragment" + e.toString());

		}

	}
}
