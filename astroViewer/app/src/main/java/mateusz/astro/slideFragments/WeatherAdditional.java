package mateusz.astro.slideFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.test.suitebuilder.TestMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import mateusz.astro.activities.MainActivity;
import mateusz.astro.adapters.DBHandler;
import mateusz.astro.astroViewerMainPackage.R;
import mateusz.models.weather.JSON.JSON.Atmosphere;
import mateusz.models.weather.JSON.JSON.Channel;
import mateusz.models.weather.JSON.JSON.Item;
import mateusz.models.weather.JSON.JSON.Location;
import mateusz.models.weather.JSON.JSON.Wind;
import mateusz.models.weather.JSON.YahooService.YahooWeatherCallback;
import mateusz.models.weather.JSON.YahooService.YahooWeatherService;

public class WeatherAdditional extends Fragment {

	static TextView myTextView;
	static SharedPreferences preferences;
	static View viewWeatherAdditional;
	static YahooWeatherService service;
	static ProgressDialog progressDialog;
	static ImageView weatherIconAdditional;
	static TextView windSpeedTextView, directionTextView, humidityTextView, visibilityTextView;

	@Override
	public View onCreateView(LayoutInflater inflater,
							 @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		viewWeatherAdditional = inflater.inflate(R.layout.pager_fragment_additional_weather_today, container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		weatherRefreshAdditionalDataFragment(getActivity());

		return viewWeatherAdditional;
	}

	public static void weatherRefreshAdditionalDataFragment(Context context) {

		try{

			weatherIconAdditional = (ImageView) viewWeatherAdditional.findViewById(R.id.additionalWeatherIconImageView);
			windSpeedTextView = (TextView) viewWeatherAdditional.findViewById(R.id.windSpeedTextView);
			directionTextView = (TextView) viewWeatherAdditional.findViewById(R.id.directionTextView);
			humidityTextView = (TextView) viewWeatherAdditional.findViewById(R.id.humidityTextView);
			visibilityTextView = (TextView) viewWeatherAdditional.findViewById(R.id.visibilityTextView);

			DBHandler db = new DBHandler(context);

			String windSpeed = db.getWindSpeed(1),
					direction = db.getDirection(1),
					humidity = db.getHumidity(1),
					visibility = db.getVisibility(1);

			Drawable weatherIconDrawable = context.getResources().getDrawable(db.getResourceIdAdditional(1));

			weatherIconAdditional.setImageDrawable(weatherIconDrawable);
			windSpeedTextView.setText(windSpeed);
			directionTextView.setText(direction);
			humidityTextView.setText(humidity);
			visibilityTextView.setText(visibility);

			Log.i ("Conditions additional ", "" + windSpeed + direction + humidity + visibility + weatherIconDrawable );

		}catch (Exception e) {

			Log.e ( "Error" , "weatherRefreshAdditionalDataFragment" + e.toString());

		}
	}
}