package mateusz.astro.slideFragments;

import mateusz.astro.astroViewerMainPackage.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Moon extends Fragment {

	static TextView myTextView;
	static SharedPreferences preferences;
	static View viewMoon;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		viewMoon = inflater.inflate(R.layout.pager_fragment_moon, container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		astroDataRefreshMoonFragment();
//		astroDataRefreshMoon();
		
		return viewMoon;
	}

	public static void astroDataRefreshMoonFragment() {

		try{
			String moonData = preferences.getString("Moon", "-");
			myTextView = (TextView) viewMoon.findViewById(R.id.moonFragmentText2);
			myTextView.setText(moonData);
			myTextView.setMovementMethod(new ScrollingMovementMethod());
		}catch (Exception e) {}

	}
/*
	private void astroDataRefreshMoon() {

		Thread myThreadData = null;
		Runnable runnableData = new astroDataGeterMoon();
		myThreadData= new Thread(runnableData);
		myThreadData.start();

	}

	class astroDataGeterMoon implements Runnable{
		public void run() {
			while(!Thread.currentThread().isInterrupted()){
				try {
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							try{
								Log.e("MOON", "--------------------------------------Moon fragment data refreshed---------------------------");
								String moonData = preferences.getString("Moon", "");
								myTextView = (TextView) v.findViewById(R.id.moonFragmentText2);
								myTextView.setText(moonData);

							} catch (Exception e) {}
						}
					});

					String refreshTime = preferences.getString("RefreshTime", "");
					Integer t = Integer.parseInt(refreshTime);

					Thread.sleep(1000 * 60);

				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}catch(Exception e){
				}
			}
		}
	}
*/
}
