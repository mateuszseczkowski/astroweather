package mateusz.astro.fragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import mateusz.astro.adapters.DBHandler;

import java.util.ArrayList;

import mateusz.astro.astroViewerMainPackage.R;
import mateusz.models.IsNetworkConnection;
import mateusz.models.weather.JSON.YahooService.YahooWeatherCallbackLocationOnly;
import mateusz.models.weather.JSON.YahooService.YahooWeatherServiceFindLocationOnly;

public class MySettings extends Fragment implements YahooWeatherCallbackLocationOnly {

    EditText longitudeTextEdit;
    EditText latitudeTextEdit;
    EditText cityTextEdit;
    TextView selectedCityText;
    SharedPreferences preferences;
    View v;
    Boolean wasSelected = false;
    public static ProgressDialog progressDialog;
    static YahooWeatherServiceFindLocationOnly service;
    private RadioGroup unitsGroup;
    private RadioButton radioUnitsButton;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_settings, container, false);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();

        String longitude = preferences.getString("Longitude", "");
        String latitude = preferences.getString("Latitude", "");
        String refreshTime = preferences.getString("RefreshTime", "");
        String cityWeatherData = preferences.getString("CityWeather", "");

        if(longitude.equals("") || longitude.length() == 0)
        {
            longitude = "0";
            editor.putString("Longitude",longitude);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        if(latitude.equals("") || latitude.length() == 0)
        {
            latitude = "0";
            editor.putString("Latitude",latitude);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        if(refreshTime.equalsIgnoreCase("") || refreshTime.length() == 0)
        {
            refreshTime = "15";
            editor.putString("RefreshTime",refreshTime);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        longitudeTextEdit = (EditText) v.findViewById(R.id.longitudeSetting);
        latitudeTextEdit = (EditText) v.findViewById(R.id.latitudeSetting);
        cityTextEdit = (EditText) v.findViewById(R.id.cityWeatherSetting);
        selectedCityText = (TextView) v.findViewById(R.id.selectedCityText);
        NumberPicker numberPicker1 = (NumberPicker) v.findViewById(R.id.numberPicker1);

        longitudeTextEdit.setText(longitude);
        latitudeTextEdit.setText(latitude);
        cityTextEdit.setText(cityWeatherData);
        selectedCityText.setText(cityWeatherData);

        try {
            numberPicker1.setValue(Integer.parseInt(refreshTime));
        } catch (Exception e) { numberPicker1.setValue(15); }

        longitudeTextEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                String longFieldText = longitudeTextEdit.getText().toString();

                int indexOfDot = longFieldText.indexOf(".");
                int indexOfMinus = longFieldText.indexOf('-');
                int i = 0;

                if (indexOfMinus == 0) {
                    i = 1;
                }
                if ((longFieldText.length() > i + 3) && indexOfDot != i + 1 && indexOfDot != i + 2 && indexOfDot != i + 3 ) {
                    String longitude = preferences.getString("Longitude", "");

                    longitudeTextEdit.setText(longitude);
                }
                else {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("Longitude", longFieldText);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                        editor.apply();
                    }
                    editor.putString("lastRefreshTime", String.valueOf("0"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                        editor.apply();
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        latitudeTextEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                String latFieldText = latitudeTextEdit.getText().toString();

                int indexOfDot = latFieldText.indexOf(".");
                int indexOfMinus = latFieldText.indexOf('-');
                int i = 0;

                if (indexOfMinus == 0) {
                    i = 1;
                }

                if ((latFieldText.length() > i + 3) && indexOfDot != i + 1 && indexOfDot != i + 2 && indexOfDot != i + 3 ) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    String latitude = preferences.getString("Latitude", "");

                    latitudeTextEdit.setText(latitude);
                }
                else {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("Latitude", latFieldText);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                        editor.apply();
                    }
                    editor.putString("lastRefreshTime", String.valueOf("0"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                        editor.apply();
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

        });

        numberPicker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {

                Integer refreshTimeInt = numberPicker.getValue();

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("RefreshTime", refreshTimeInt.toString());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    editor.apply();
                }
            }
        });

        initSelectCityButton();
        initListOfCities();
        initDeleteCityButton();
        onRadioButtonClicked();

        return v;
    }

    private void initSelectCityButton() {

        v.findViewById(R.id.selectCityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("CityWeather", cityTextEdit.getText().toString());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    editor.apply();
                }

                String cityWeatherData = preferences.getString("CityWeather", "");
                cityWeatherData = cityWeatherData.toLowerCase();

                findWeatherLocation(cityWeatherData);

            }
        });
    }

    private void initDeleteCityButton() {

        v.findViewById(R.id.deleteCityButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String cityWeatherData = cityTextEdit.getText().toString();
                cityWeatherData = cityWeatherData.toLowerCase();

                DBHandler db = new DBHandler(getActivity());
                if (db.getCountCity(cityWeatherData) == 0 ) {
                    Toast.makeText(getActivity(), "City does not exist on the list", Toast.LENGTH_SHORT).show();
                }
                else {
                    db.deleteCity(cityWeatherData);
                    cityTextEdit.setText("");
                    initListOfCities();
                }
            }
        });
    }

    private void initListOfCities() {

        DBHandler db = new DBHandler(getActivity());
        ArrayList<String> citiesList = db.getCitiesList();

        ListView lv = (ListView) v.findViewById(R.id.list_of_cities);
        lv.setAdapter(new ArrayAdapter <String> (getActivity(), R.layout.cities_list_single_item, citiesList));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView <? > arg0, View view, int position, long id) {
                cityTextEdit.setText(((TextView) view).getText());
                wasSelected = true;
            }
        });

        lv.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                v.onTouchEvent(event);
                return true;
            }
        });
    }

    private void findWeatherLocation(String city) {
        DBHandler db = new DBHandler(getActivity());
        Log.i("Looking for location: ",city);
        IsNetworkConnection isNetworkConnection = new IsNetworkConnection(getActivity());

        if (db.getCountCity(city) == 0 && isNetworkConnection.getIsNetworkConnection()){
            service = new YahooWeatherServiceFindLocationOnly(this);
            service.findLocation(city);
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Looking for location: " + city + "...");
            progressDialog.show();
        }
        else if (wasSelected){
            wasSelected = false;
            setTextSelectedCity(db, city);
        }
        else if (db.getCountCity(city) > 0 && !wasSelected) {
            Toast.makeText(getActivity(), "City already exists on the list or is selected", Toast.LENGTH_SHORT).show();
            setTextSelectedCity(db, city);
        }
        else if (!isNetworkConnection.getIsNetworkConnection()) {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void serviceSuccess(String location) {
        progressDialog.hide();
        try {
            DBHandler db = new DBHandler(getActivity());
            location = location.toLowerCase();
            db.addDataCity(2, location);
            initListOfCities();
            setTextSelectedCity(db, location);
            Toast.makeText(getActivity(), "Location was added to the list and selected successfully", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            Log.e("Something went wrong: ",e.getMessage());
        }
    }

    @Override
    public void serviceFail(Exception exception) {
        progressDialog.hide();
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void setTextSelectedCity (DBHandler db, String cityWeatherData) {
        selectedCityText = (TextView) v.findViewById(R.id.selectedCityText);
        selectedCityText.setText(db.getCity(cityWeatherData));
        resetLastRefreshTime();
    }

    public void onRadioButtonClicked() {

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String units = preferences.getString("UnitsOption", "");
        RadioButton imperialRadioButton = (RadioButton) v.findViewById(R.id.imperialRadioButton);
        RadioButton metricRadioButton = (RadioButton) v.findViewById(R.id.metricRadioButton);

        if (units.equals("c")) {
            imperialRadioButton.setChecked(false);
            metricRadioButton.setChecked(true);
        }
        else {
            imperialRadioButton.setChecked(true);
            metricRadioButton.setChecked(false);
        }

        unitsGroup = (RadioGroup) v.findViewById(R.id.unitsGroup);

        v.findViewById(R.id.selectUnitsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = unitsGroup.getCheckedRadioButtonId();
                SharedPreferences.Editor editor = preferences.edit();
                String text = "f";

                radioUnitsButton = (RadioButton) v.findViewById(selectedId);

                if (radioUnitsButton.getText().toString().contains("Metric"))  {
                    text = "c";
                }

                editor.putString("UnitsOption", text);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    editor.apply();
                }
                Toast.makeText(getActivity(), radioUnitsButton.getText().toString() + " is chosen", Toast.LENGTH_LONG).show();
                resetLastRefreshTime();
            }
        });
    }

    private void resetLastRefreshTime () {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastRefreshTime", String.valueOf(1));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        }
    }
}
