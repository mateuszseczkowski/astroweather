package mateusz.astro.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import mateusz.astro.adapters.AstroFragmentPageAdapter;
import mateusz.astro.astroViewerMainPackage.R;
import mateusz.astro.slideFragments.Moon;
import mateusz.astro.slideFragments.Sun;
import mateusz.astro.slideFragments.WeatherAdditional;
import mateusz.astro.slideFragments.WeatherBasic;
import mateusz.astro.slideFragments.WeatherNextDays;
import mateusz.models.AstroGetData;
import mateusz.models.IsNetworkConnection;
import mateusz.models.WeatherGetDataAndSaveToDB;
import mateusz.models.weather.JSON.JSON.Channel;
import mateusz.models.weather.JSON.YahooService.YahooWeatherCallback;
import mateusz.models.weather.JSON.YahooService.YahooWeatherService;

public class MyHome extends Fragment  implements
        TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener, YahooWeatherCallback {

    private TabHost tabHost;
    private ViewPager viewPager;
    private AstroFragmentPageAdapter myViewPagerAdapter;
    View v;
    private TextView updateDateTextField;
    public static ProgressDialog progressDialog;
    static YahooWeatherService service;
    SharedPreferences preferences;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.astro_slide_layout, container, false);

 /*       if ((getResources().getConfiguration().screenLayout
                & getResources().getConfiguration().SCREENLAYOUT_SIZE_MASK)
                != 4) {
            if (getResources().getConfiguration().orientation == 1) { */
                this.initTabHost(savedInstanceState);
                this.initViewPager();
  /*          }
        }
        */

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        getLongLat();
        date();
        astroDataRefresh();
        initRefreshButton();

        return v;
    }

    private void initRefreshButton() {

        v.findViewById(R.id.refreshButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread refreshDataThread = null;
                Runnable runnable = new Runnable() {
                    public void run() {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("lastRefreshTime", String.valueOf(12));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                            editor.apply();
                        }
                        astroDataRefresh();
                    }
                };
                refreshDataThread = new Thread(runnable);
                refreshDataThread.start();
            }
        });
    }

    private void astroDataRefresh() {

        Thread myThreadData = null;
        Runnable runnableData = new astroDataGeter();
        myThreadData= new Thread(runnableData);
        myThreadData.start();

    }

    class astroDataGeter implements Runnable{
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            try{

                                String lastRefreshTime = preferences.getString("lastRefreshTime", "");
                                long curTime = (new Date().getTime());
                                long diff = 60000000;
                                long lastRefreshTimeLong;

                                try {
                                    lastRefreshTimeLong = Long.parseLong(lastRefreshTime);
                                    diff = Math.abs(curTime) - Math.abs(lastRefreshTimeLong);
                                } catch (Exception e) {}

                                String refreshTime = preferences.getString("RefreshTime", "15");
                                Integer t = Integer.parseInt(refreshTime);

                                long  howManyMinutesAgoDataWasRefreshed =  Math.abs(diff/59100);

                                if (howManyMinutesAgoDataWasRefreshed >= (long) t) {
                                    initDataMoonSun();

                                    IsNetworkConnection isNetworkConnection = new IsNetworkConnection(getActivity());
                                    if (isNetworkConnection.getIsNetworkConnection()) {
                                        initDataWeather();
                                    }
                                    else {
                                        Toast.makeText(getActivity(), "No internet connection. Data will be downloaded from data base.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                astroGetSavedData();

                            }catch (Exception e) {}
                        }
                    });

                    Thread.sleep(1000*60);

                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

    private void astroGetSavedData() {
/*
        if ((getResources().getConfiguration().screenLayout & getResources().getConfiguration().SCREENLAYOUT_SIZE_MASK)
                != 1) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String moonData = preferences.getString("Moon", "-");
        String sunData = preferences.getString("Sun", "-");

            try {
                moonFragmentTextContent = (TextView) v.findViewById(R.id.moonFragmentText2);
                sunFragmentTextContent = (TextView) v.findViewById(R.id.sunFragmentText2);
                moonFragmentTextContent.setText(moonData);
                sunFragmentTextContent.setText(sunData);
            } catch (Exception e) {}

        }*/

        Moon.astroDataRefreshMoonFragment();
        Sun.astroDataRefreshSunFragment();

        WeatherBasic.weatherRefreshBasicDataFragment(getActivity());
        WeatherAdditional.weatherRefreshAdditionalDataFragment(getActivity());
        WeatherNextDays.weatherRefreshNextDaysDataFragment(getActivity());

        this.getLastUpdateDate();
    }

    public void getLastUpdateDate() {

        try{
            String updateDate = preferences.getString("lastUpdateDate", "-");
            updateDateTextField = (TextView) v.findViewById(R.id.lastUpdate);
            updateDateTextField.setText(updateDate);
            updateDateTextField.setMovementMethod(new ScrollingMovementMethod());
        }catch (Exception e) {}

    }

    private void date() {

        Thread myThread = null;
        Runnable runnable = new dateTime();
        myThread= new Thread(runnable);
        myThread.start();

    }

    class dateTime implements Runnable{
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            try{
                                TextView txtCurrentTime= (TextView) v.findViewById(R.id.date);
                                Date dt = new Date();
                                String curTime = dt.toString();
                                txtCurrentTime.setText(curTime);
                            }catch (Exception e) {}
                        }
                    });

                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }catch(Exception e){
                }
            }
        }
    }

    private void getLongLat() {

        TextView currentLongLang= (TextView) v.findViewById(R.id.place);

        String longitude = preferences.getString("Longitude", "0");
        String latitude = preferences.getString("Latitude", "0");

        currentLongLang.setText("Lat: " + longitude + " Long: " + latitude);
    }

    private void initDataMoonSun() {

        SharedPreferences.Editor editor = preferences.edit();

        String longitude = preferences.getString("Longitude", "");
        String latitude = preferences.getString("Latitude", "");
        String refreshTime = preferences.getString("RefreshTime", "");

        if(longitude.equals("") || longitude.length() == 0)
        {
            longitude = "0";
            editor.putString("Longitude",longitude);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        if(latitude.equals("") || latitude.length() == 0)
        {
            latitude = "0";
            editor.putString("Latitude",latitude);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        if(refreshTime.equalsIgnoreCase("") || refreshTime.length() == 0)
        {
            refreshTime = "15";
            editor.putString("RefreshTime",refreshTime);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }
        }

        try {

            long intCurTime = new Date().getTime();
            String dateString = new Date().toString();

            editor.putString("lastRefreshTime", String.valueOf(intCurTime));
            editor.putString("lastUpdateDate", dateString);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }

            Double longDouble = Double.parseDouble(longitude);
            Double latDouble = Double.parseDouble(latitude);

            AstroGetData newData = new AstroGetData(longDouble, latDouble);

            editor.putString("Moon",newData.astroMoon());
            editor.putString("Sun", newData.astroSun());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                editor.apply();
            }

        } catch (Exception e) {}

    }

    private void initDataWeather() {

        service = new YahooWeatherService(this);
        String cityWeatherData = preferences.getString("CityWeather", "");
        String units = preferences.getString("UnitsOption", "f");
        service.downloadWeatherData(cityWeatherData, units);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading data...");
        progressDialog.show();

    }

    @Override
    public void serviceSuccess(Channel channel) {
        progressDialog.hide();
        WeatherGetDataAndSaveToDB weatherData = new WeatherGetDataAndSaveToDB(channel, getActivity());
        weatherData.getDataAndSaveInDB();
        astroGetSavedData();
    }

    @Override
    public void serviceFail(Exception exception) {
        progressDialog.hide();
        Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    class FakeContent implements TabHost.TabContentFactory {
        private final Context mContext;

        public FakeContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumHeight(0);
            v.setMinimumWidth(0);
            return v;
        }
    }

    private void initViewPager() {
        List<Fragment> fragments = new Vector<Fragment>();

        Fragment moonFrag = new Moon();
        Fragment sunFrag = new Sun();

        fragments.add(new WeatherBasic());
        fragments.add(new WeatherAdditional());
        fragments.add(new WeatherNextDays());
        fragments.add(moonFrag);
        fragments.add(sunFrag);

        this.myViewPagerAdapter = new AstroFragmentPageAdapter(
                getChildFragmentManager(), fragments);
        this.viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        this.viewPager.setAdapter(this.myViewPagerAdapter);
        this.viewPager.setOnPageChangeListener(this);

    }

    private void initTabHost(Bundle args) {

        tabHost = (TabHost) v.findViewById(android.R.id.tabhost);
        tabHost.setup();

        String[] namesOfFragments = {"Basic Weather", "Pro Weather", "Next Days Weather", "Moon", "Sun"};

        for (String fragmentName : namesOfFragments) {
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(fragmentName);
            tabSpec.setIndicator(fragmentName);
            tabSpec.setContent(new FakeContent(getActivity()));
            tabHost.addTab(tabSpec);
        }
        tabHost.setOnTabChangedListener(this);
    }

    @Override
    public void onTabChanged(String tabId) {
        int pos = this.tabHost.getCurrentTab();
        this.viewPager.setCurrentItem(pos);

        HorizontalScrollView hScrollView = (HorizontalScrollView) v.findViewById(R.id.hScrollView);
        View tabView = tabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft()
                - (hScrollView.getWidth() - tabView.getWidth()) / 2;
        hScrollView.smoothScrollTo(scrollPos, 0);

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override
    public void onPageSelected(int position) {
        this.tabHost.setCurrentTab(position);
    }
}
